package com.kouroshraeen.databindingsampleapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.kouroshraeen.databindingsampleapp.adapter.BillsAdapter;
import com.kouroshraeen.databindingsampleapp.api.ApiClient;
import com.kouroshraeen.databindingsampleapp.api.OpenStatesApi;
import com.kouroshraeen.databindingsampleapp.model.Bill;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    List<Bill> mBills;
    RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        OpenStatesApi apiService = ApiClient.getApiClient();

        Call<List<Bill>> call =  apiService.getBills("ca", "session", "", "created_at", BuildConfig.OPEN_STATES_API_KEY);

        call.enqueue(new Callback<List<Bill>>() {
            @Override
            public void onResponse(Call<List<Bill>> call, Response<List<Bill>> response) {
                mBills = response.body();
                updateUi();
            }

            @Override
            public void onFailure(Call<List<Bill>> call, Throwable t) {
                Log.d(TAG, t.getMessage());
            }
        });
    }

    private void updateUi() {
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, R.drawable.divider));
        BillsAdapter adapter = new BillsAdapter(mBills);
        mRecyclerView.setAdapter(adapter);
    }
}
