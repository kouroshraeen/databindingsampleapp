package com.kouroshraeen.databindingsampleapp.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.kouroshraeen.databindingsampleapp.R;
import com.kouroshraeen.databindingsampleapp.databinding.BillListItemBinding;
import com.kouroshraeen.databindingsampleapp.model.Bill;
import com.kouroshraeen.databindingsampleapp.util.Utils;

import java.util.List;


public class BillsAdapter extends RecyclerView.Adapter<BillsAdapter.BillViewHolder> {

    private List<Bill> mBills;

    public BillsAdapter(List<Bill> bills) {
        mBills = bills;
    }


    @Override
    public BillViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);
        BillListItemBinding binding = DataBindingUtil.inflate(inflater, R.layout.bill_list_item, parent, false);

        return new BillViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(BillViewHolder holder, int position) {
        holder.bind(position);
    }

    @Override
    public int getItemCount() {
        if (mBills != null) {
            return mBills.size();
        } else {
            return 0;
        }
    }


    class BillViewHolder extends RecyclerView.ViewHolder {
        private final BillListItemBinding mBinding;

        BillViewHolder(BillListItemBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        void bind(int position) {
            Bill bill = mBills.get(position);
            mBinding.billDate.setText(Utils.getFormattedDateString(bill.getCreatedAt()));
            mBinding.billId.setText(bill.getBillId());
            mBinding.billTitle.setText(bill.getTitle());
        }
    }
}
